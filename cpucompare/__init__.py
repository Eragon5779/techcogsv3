from .cpucompare import CPUCompare
from redbot.core import data_manager


async def setup(bot):
    n = CPUCompare(bot)
    await n.load_data()
    bot.add_cog(n)
