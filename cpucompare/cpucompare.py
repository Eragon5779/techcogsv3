# Discord Imports
import discord
from redbot.core import commands
from discord import Embed

# Data processing imports
import json
from redbot.core import Config
from redbot.core.data_manager import bundled_data_path

# Exception imports
from concurrent.futures import TimeoutError

"""
Based on my earlier TechBot (https://gitlab.com/Eragon5779/TechBot)
"""


def process_intel(data, compare=False):

    wantedData = {
        "Cores": data["CoreCount"] if "CoreCount" in data.keys() else "N/A",
        "Threads": data["ThreadCount"] if "ThreadCount" in data.keys() else "N/A",
        "Base Clock": data["ClockSpeed"] if "ClockSpeed" in data.keys() else "N/A",
        "Boost Clock": data["ClockSpeedMax"]
        if "ClockSpeedMax" in data.keys()
        else "N/A",
        "Cache": data["Cache"] if "Cache" in data.keys() else "N/A",
        "TDP": data["MaxTDP"] if "MaxTDP" in data.keys() else "N/A",
        "Release Date": data["BornOnDate"] if "BornOnDate" in data.keys() else "N/A",
        "Max Memory": data["MaxMem"] if "MaxMem" in data.keys() else "N/A",
        "Lithography": data["Lithography"] if "Lithography" in data.keys() else "N/A",
        "Socket": data["SocketsSupported"]
        if "SocketsSupported" in data.keys()
        else "N/A",
        "Multiple CPU": data["ScalableSockets"]
        if "ScalableSockets" in data.keys()
        else "N/A",
    }
    if compare:
        wantedData = {
            "cores": data["CoreCount"] if "CoreCount" in data.keys() else "N/A",
            "threads": data["ThreadCount"] if "ThreadCount" in data.keys() else "N/A",
            "base_clock": data["ClockSpeed"] if "ClockSpeed" in data.keys() else "N/A",
            "boost_clock": data["ClockSpeedMax"]
            if "ClockSpeedMax" in data.keys()
            else "N/A",
            "cache": data["Cache"] if "Cache" in data.keys() else "N/A",
            "tdp": data["MaxTDP"] if "MaxTDP" in data.keys() else "N/A",
            "release_date": data["BornOnDate"]
            if "BornOnDate" in data.keys()
            else "N/A",
        }
    return wantedData


def process_amd(data, compare=False):
    wantedData = {
        "Cores": data["Core Count"] if "Core Count" in data.keys() else "N/A",
        "Threads": data["Thread Count"] if "Thread Count" in data.keys() else "N/A",
        "Base Clock": data["Base Frequency"]
        if "Base Frequency" in data.keys()
        else "N/A",
        "Boost Clock": data["Boost Frequency"]
        if "Boost Frequency" in data.keys()
        else "N/A",
        "Cache": data["L3 Cache (Total)"]
        if "L3 Cache (Total)" in data.keys()
        else "N/A",
        "TDP": data["TDP"] if "TDP" in data.keys() else "N/A",
        "Release Date": data["Release Date"]
        if "Release Date" in data.keys()
        else "N/A",
        "Lithography": data["Lithography"] if "Lithography" in data.keys() else "N/A",
        "Socket": data["Socket"] if "Socket" in data.keys() else "N/A",
    }
    if compare:
        wantedData = {
            "cores": data["Core Count"] if "Core Count" in data.keys() else "N/A",
            "threads": data["Thread Count"] if "Thread Count" in data.keys() else "N/A",
            "base_clock": data["Base Frequency"]
            if "Base Frequency" in data.keys()
            else "N/A",
            "boost_clock": data["Boost Frequency"]
            if "Boost Frequency" in data.keys()
            else "N/A",
            "cache": data["L3 Cache (Total)"]
            if "L3 Cache (Total)" in data.keys()
            else "N/A",
            "tdp": data["TDP"] if "TDP" in data.keys() else "N/A",
            "release_date": data["Release Date"]
            if "Release Date" in data.keys()
            else "N/A",
        }
    return wantedData


def compare(cpu1, cpu2, cpu1_name, cpu2_name):
    """Create table for 2 CPU comparison"""
    cpu1_data, cpu2_data = {}, {}
    cpu1_brand, cpu2_brand = "", ""
    if "Intel" in cpu1_name:
        cpu1_data = process_intel(cpu1, True)
        cpu1_name = cpu1_name.replace("Processor ", "").split("Intel®")[1].split("(")[0]
        if "Extreme" in cpu1_name:
            cpu1_name = cpu1_name.split("Extreme")[0]
        cpu1_brand = "Intel"
    else:
        cpu1_data = process_amd(cpu1["data"], True)
        cpu1_brand = "AMD"
    if "Intel" in cpu2_name:
        cpu2_data = process_intel(cpu2, True)
        cpu2_name = cpu2_name.replace("Processor ", "").split("Intel®")[1].split("(")[0]
        cpu2_brand = "Intel"
    else:
        cpu2_data = process_amd(cpu2["data"], True)
        cpu2_brand = "AMD"
    for f in cpu1_data:
        if cpu1_data[f] == None or (
            type(cpu1_data[f]) == str and cpu1_data[f].strip() == ""
        ):
            cpu1_data[f] = "N/A"
    for f in cpu2_data:
        if cpu2_data[f] == None or (
            type(cpu2_data[f]) == str and cpu2_data[f].strip() == ""
        ):
            cpu2_data[f] = "N/A"

    # Assemble the embed.
    embed = Embed(title="Comparison", color=0xFFFF00)
    embed.add_field(value="CPU", name="Brand")
    embed.add_field(value=cpu1_name, name=cpu1_brand)
    embed.add_field(value=cpu2_name, name=cpu2_brand)
    embed.add_field(name="Cores", value="Threads")
    embed.add_field(name=cpu1_data["cores"], value=cpu1_data["threads"])
    embed.add_field(name=cpu2_data["cores"], value=cpu2_data["threads"])
    embed.add_field(name="Base Clock", value="Boost Clock")
    embed.add_field(name=cpu1_data["base_clock"], value=cpu1_data["boost_clock"])
    embed.add_field(name=cpu2_data["base_clock"], value=cpu2_data["boost_clock"])
    embed.add_field(value="Size", name="Cache")
    embed.add_field(value=cpu1_data["cache"], name="L3")
    embed.add_field(value=cpu2_data["cache"], name="L3")
    embed.add_field(name="TDP", value="TDie")
    embed.add_field(name=cpu1_data["tdp"], value="N/A")
    embed.add_field(name=cpu2_data["tdp"], value="N/A")
    embed.add_field(name="Status", value="Date")
    embed.add_field(value=cpu1_data["release_date"], name="Released")
    embed.add_field(value=cpu2_data["release_date"], name="Released")
    return embed


class CPUCompare(commands.Cog):
    """A cog that retrieves and compares CPU specs"""

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(self, 8190975042)
        self.config.register_global(intel={}, amd={}, geekbench={})

    async def load_data(self):
        """Load data from files into arrays"""
        intelfp = bundled_data_path(self) / "intel.json"
        amdfp = bundled_data_path(self) / "amd.json"
        gbfp = bundled_data_path(self) / "geekbench.json"
        with intelfp.open("rt") as f:
            await self.config.intel.set(json.loads(f.read()))
        with amdfp.open("rt") as f:
            await self.config.amd.set(json.loads(f.read()))
        with gbfp.open("rt") as f:
            await self.config.geekbench.set(json.loads(f.read()))

    async def gb_search(self, search):
        async with self.config.geekbench() as gb:
            matching = []
            cpuList = ""
            count = 0
            for cpu in gb:
                if search.lower() in cpu.lower():
                    matching.append(cpu)
            matching.sort()
            for cpu in matching:
                cpuList += "[%d] %s\n" % (count, cpu)
                count += 1
            return matching, cpuList, count

    async def amd_search(self, search):
        """Search through AMD specs file for needed data"""
        async with self.config.amd() as amd:
            matching = []
            cpuList = ""
            count = 0
            for cpu in amd.keys():
                if search.lower() in amd[cpu]["humanName"].lower():
                    matching.append(cpu)
            matching.sort()
            for cpu in matching:
                cpuList += "[%d] %s\n" % (count, amd[cpu]["humanName"])
                count += 1
        return matching, cpuList, count

    async def intel_search(self, search):
        """Search through Intel specs file for needed data"""
        async with self.config.intel() as intel:
            matching = []
            cpuList = ""
            count = 0
            for cpu in intel.keys():
                if search.lower() in cpu.lower().replace("®", "").replace("™", ""):
                    matching.append(cpu)
            matching.sort()
            for cpu in matching:
                cpuList += "[%d] %s\n" % (count, cpu)
                count += 1
        return matching, cpuList, count

    @commands.command(name="intel")
    async def _intel(self, ctx: commands.Context, *, arg=""):
        """Retrieves requested Intel CPU and returns an embed with the specs"""

        def same_user_check(m):
            return m.author == ctx.message.author and m.channel == ctx.message.channel

        async with self.config.intel() as intel:
            search = arg
            if not search:
                await ctx.send("Please enter model you want to search for (i.e. 9700)")
                try:
                    msg = await self.bot.wait_for(
                        "message", check=same_user_check, timeout=30.0
                    )
                except TimeoutError:
                    await ctx.send("Timeout reached. Please restart your search")
                    return
                search = msg.content
            matching, cpuList, count = await self.intel_search(search)
            if len(cpuList) < 1:
                await ctx.send("No processors found")
                return
            try:
                await ctx.send(cpuList)
            except:
                await ctx.send("Error: List too large. Please refine search")
                return
            choice = 0
            if count > 1:
                await ctx.send("Which CPU do you want?")
                try:
                    msg = await self.bot.wait_for(
                        "message", check=same_user_check, timeout=30.0
                    )
                except TimeoutError:
                    await ctx.send("Timeout reached. Please restart your search")
                    return
                try:
                    choice = int(msg.content)
                    if choice > count or choice < 0:
                        raise ValueError("Out of Range")
                except ValueError:
                    await ctx.send(
                        "Error: Choice out of range. Please restart your search"
                    )
                    return
                except:
                    await ctx.send("Error: Not a number. Please restart your search.")
                    return
            cpu_data = intel[matching[choice]]
            cpu_name = (
                matching[choice]
                .replace("Processor ", "")
                .split("Intel®")[1]
                .split("(")[0]
                .strip()
            )
            embed = discord.Embed(title=cpu_name, color=0x0000FF)
            wantedData = process_intel(cpu_data)
            for field in wantedData.keys():
                if type(wantedData[field]) == str and wantedData[field].strip() == "":
                    wantedData[field] = "N/A"
                elif not wantedData[field]:
                    wantedData[field] = "N/A"
                embed.add_field(name=field, value=wantedData[field])
        await ctx.send(embed=embed)

    @commands.command(name="geekbench")
    async def _geekbench(self, ctx: commands.Context, *, arg=""):
        """A command to get GeekBench scores for specified CPUs"""

        def same_user_check(m):
            return m.author == ctx.message.author and m.channel == ctx.message.channel

        async with self.config.geekbench() as gb:
            search = arg
            if not search:
                await ctx.send("Please enter model you want to search for (i.e. 2700)")
                try:
                    msg = await self.bot.wait_for(
                        "message", check=same_user_check, timeout=30.0
                    )
                except TimeoutError:
                    await ctx.send("Timeout reached. Please restart your search")
                    return

        matching, cpuList, count = await self.gb_search(search=search)

        if len(cpuList) < 1:
            await ctx.send("No processors found")
            return
        try:
            await ctx.send(cpuList)
        except:
            await ctx.send("Error: List too large. Please refine search")
            return
        choice = 0
        if count > 1:
            await ctx.send("Which CPU do you want?")
            try:
                msg = await self.bot.wait_for(
                    "message", check=same_user_check, timeout=30.0
                )
            except TimeoutError:
                await ctx.send("Timeout reached. Please restart your search")
                return
            try:
                choice = int(msg.content)
                if choice > count or choice < 0:
                    raise ValueError("Out of Range")
            except ValueError:
                await ctx.send("Error: Out of Range. Please restart your search.")
                return
            except:
                await ctx.send("Error: Not a number. Please restart your search.")
                return
        gb_data = gb[matching[choice]]
        final_data = {}
        wanted = ["samples", "score", "multicore_score"]
        embed = discord.Embed(title=matching[choice], color=0x067FC0)
        for field in wanted:
            if not gb_data[field]:
                final_data[field] = "N/A"
            else:
                final_data[field] = gb_data[field]
            embed.add_field(
                name=" ".join(field.split("_")).title(), value=final_data[field]
            )
        await ctx.send(embed=embed)

    @commands.command(name="amd")
    async def _amd(self, ctx: commands.Context, *, arg=""):
        """Retrieves requested AMD CPU and returns an embed with the specs"""

        def same_user_check(m):
            return m.author == ctx.message.author and m.channel == ctx.message.channel

        async with self.config.amd() as amd:
            search = arg
            if not search:
                await ctx.send("Please enter model you want to search for (i.e. 2700)")

                try:
                    msg = await self.bot.wait_for(
                        "message", check=same_user_check, timeout=30.0
                    )
                except TimeoutError:
                    await ctx.send("Timeout reached. Please restart your search")
                    return
                search = msg.content

            matching, cpuList, count = await self.amd_search(search=search)

            if len(cpuList) < 1:
                await ctx.send("No processors found")
                return
            try:
                await ctx.send(cpuList)
            except:
                await ctx.send("Error: List too large. Please refine search.")
                return
            choice = 0
            if count > 1:
                await ctx.send("Which CPU do you want?")
                try:
                    msg = await self.bot.wait_for(
                        "message", check=same_user_check, timeout=30.0
                    )
                except TimeoutError:
                    await ctx.send("Timeout reached. Please restart your search")
                    return
                try:
                    choice = int(msg.content)
                    if choice > count or choice < 0:
                        raise ValueError("Out of Range")
                except ValueError:
                    await ctx.send("Error: Out of Range. Please restart your search.")
                    return
                except:
                    await ctx.send("Error: Not a number. Please restart your search.")
                    return
            cpu_data = amd[matching[choice]]["data"]
            wantedData = process_amd(cpu_data)
            embed = discord.Embed(title=matching[choice], color=0xFF0000)
            for field in wantedData.keys():
                if type(wantedData[field]) == str and wantedData[field].strip() == "":
                    wantedData[field] = "N/A"
                elif not wantedData[field]:
                    wantedData[field] = "N/A"
                embed.add_field(name=field, value=wantedData[field])
        await ctx.send(embed=embed)

    @commands.command(name="compare")
    async def _compare(
        self, ctx: commands.Context, brand1=None, cpu1=None, brand2=None, cpu2=None
    ):
        """Compares 2 specified CPUs"""

        def same_user_check(m):
            return m.author == ctx.message.author and m.channel == ctx.message.channel

        async with self.config.intel() as intel:
            async with self.config.amd() as amd:
                # Constants for easy lookup
                searches = {"amd": self.amd_search, "intel": self.intel_search}
                lists = {"amd": amd, "intel": intel}

                # Arg sanitation
                if brand1 and brand1.lower() not in ["intel", "amd"]:
                    if cpu1 in ["intel", "amd"]:
                        if not brand2:
                            brand2 = cpu1
                        elif brand2:
                            if brand2 not in ["intel", "amd"]:
                                cpu2 = brand2
                                brand2 = cpu1
                    else:
                        cpu2 = cpu1
                    cpu1 = brand1
                    brand1 = None
                if cpu1 and cpu1.lower() in ["intel", "amd"]:
                    if brand1:
                        brand2 = cpu1
                    else:
                        brand1 = cpu1
                    cpu1 = None
                if brand2 and brand2.lower() not in ["intel", "amd"]:
                    if not cpu2:
                        cpu2 = brand2
                    brand2 = None
                if cpu2 and cpu2.lower() in ["intel", "amd"]:
                    if brand1:
                        brand2 = cpu2
                    else:
                        brand1 = cpu2
                    cpu2 = None

                # CPU 1 search
                if not brand1:
                    await ctx.send(
                        "Which brand would you like for the first CPU? (Intel or AMD)"
                    )
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    brand1 = msg.content.lower()
                if brand1.lower() not in ["intel", "amd"]:
                    await ctx.send(
                        f"{brand1} is not supported. Please try again with Intel/AMD."
                    )
                    return
                if not cpu1:
                    await ctx.send(f"Please enter search term for {brand1}: ")
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    cpu1 = msg.content

                brand1_lower = brand1.lower()

                matching, cpuList, count = await searches[brand1_lower](cpu1)
                if len(cpuList) < 1:
                    await ctx.send(f"{brand1} {cpu1} not found")
                    return
                try:
                    await ctx.send(cpuList)
                except:
                    await ctx.send("Error: List too large. Please refine search.")
                    return
                choice = 0

                if count > 1:
                    await ctx.send("Which CPU do you want?")
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    try:
                        choice = int(msg.content)
                        if choice > count or choice < 0:
                            raise ValueError("Out of Range")
                    except ValueError:
                        await ctx.send(
                            "Error: Out of Range. Please restart your search."
                        )
                    except:
                        await ctx.send(
                            "Error: Not a number. Please restart your search."
                        )
                        return

                cpu1 = lists[brand1_lower][matching[choice]]
                cpu1_name = matching[choice]

                # CPU 2 search
                if not brand2:
                    await ctx.send(
                        "Which brand would you like for the second CPU? (Intel or AMD)"
                    )
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    brand2 = msg.content.lower()
                if brand2.lower() not in ["intel", "amd"]:
                    await ctx.send(
                        f"{brand2} is not supported. Please try again with Intel/AMD."
                    )
                    return
                if not cpu2:
                    await ctx.send(f"Please enter search term for {brand2}: ")
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    cpu2 = msg.content

                brand2_lower = brand2.lower()

                matching, cpuList, count = await searches[brand2_lower](cpu2)
                if len(cpuList) < 1:
                    await ctx.send(f"{brand2} {cpu2} not found")
                    return
                try:
                    await ctx.send(cpuList)
                except:
                    await ctx.send("Error: List too large. Please refine search.")
                    return
                choice = 0

                if count > 1:
                    await ctx.send("Which CPU do you want?")
                    try:
                        msg = await self.bot.wait_for(
                            "message", check=same_user_check, timeout=30.0
                        )
                    except TimeoutError:
                        await ctx.send("Timeout reached. Please restart your search")
                        return
                    try:
                        choice = int(msg.content)
                        if choice > count or choice < 0:
                            raise ValueError("Out of Range")
                    except ValueError:
                        await ctx.send(
                            "Error: Out of Range. Please restart your search."
                        )
                    except:
                        await ctx.send(
                            "Error: Not a number. Please restart your search."
                        )
                        return

                cpu2 = lists[brand2_lower][matching[choice]]
                cpu2_name = matching[choice]

                tbl = compare(cpu1, cpu2, cpu1_name, cpu2_name)
                await ctx.send(embed=tbl)
