# Primary Imports
import base64

# Discord Imports
from redbot.core import commands

__author__ = "Eragon5779"
__version__ = "1.0"


class Encode(commands.Cog):
    """A cog wrapper for the native base64 encoding library"""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(no_pm=True)
    async def encode(self, ctx):
        """Encodes text with specified encoding method"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help()

    @encode.command()
    async def b64e(self, ctx, *, data: str):
        """Base64 encoding"""
        await ctx.send(base64.b64encode(data.encode("UTF-8")).decode("UTF-8"))

    @encode.command()
    async def b16e(self, ctx, *, data: str):
        """Base16 encoding"""
        await ctx.send(base64.b16encode(data.encode("UTF-8")).decode("UTF-8"))

    @encode.command()
    async def b32e(self, ctx, *, data: str):
        """Base32 encoding"""
        await ctx.send(base64.b32encode(data.encode("UTF-8")).decode("UTF-8"))

    @encode.command()
    async def a85e(self, ctx, *, data: str):
        """ASCII85 encoding"""
        await ctx.send(base64.a85encode(data.encode("UTF-8")).decode("UTF-8"))

    @encode.command()
    async def b85e(self, ctx, *, data: str):
        """Base85 encoding"""
        await ctx.send(base64.b85encode(data.encode("UTF-8")).decode("UTF-8"))

    @commands.group(no_pm=True)
    async def decode(self, ctx):
        """Decodes text with specified encoding method"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help()

    @decode.command()
    async def b64d(self, ctx, *, data: str):
        """Base64 decoding"""
        await ctx.send(base64.b64decode(data.encode("UTF-8")).decode("UTF-8"))

    @decode.command()
    async def b16d(self, ctx, *, data: str):
        """Base16 decoding"""
        await ctx.send(base64.b16decode(data.encode("UTF-8")).decode("UTF-8"))

    @decode.command()
    async def b32d(self, ctx, *, data: str):
        """Base32 decoding"""
        await ctx.send(base64.b32decode(data.encode("UTF-8")).decode("UTF-8"))

    @decode.command()
    async def a85d(self, ctx, *, data: str):
        """ASCII85 decoding"""
        await ctx.send(base64.a85decode(data.encode("UTF-8")).decode("UTF-8"))

    @decode.command()
    async def b85d(self, ctx, *, data: str):
        """Base85 decoding"""
        await ctx.send(base64.b85decode(data.encode("UTF-8")).decode("UTF-8"))
