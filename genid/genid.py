# Primary imports
import ulid as u
import uuid as d
from bson import ObjectId
import re

# Discord Imports
from redbot.core import commands

__author__ = "Eragon5779"
__version__ = "1.0"

OID_VERIFY = re.compile(r"^([1-9][0-9]{0,3}|0)(\.([1-9][0-9]{0,3}|0)){5,13}$")
URL_VERIFY = re.compile(
    r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]"
    + r"|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
)
DN_VERIFY = re.compile(
    r"^(?:(?P<cn>CN=(?P<name>[^,]*)),)"
    + r"?(?:(?P<path>(?:(?:CN|OU)=[^,]+,?)+),)"
    + r"?(?P<domain>(?:DC=[^,]+,?)+)$"
)
ULID_VERIFY = re.compile(r"^[0-9a-z]{26}$", re.IGNORECASE)
UUID_VERIFY = re.compile(
    (
        "[a-f0-9]{8}-"
        + "[a-f0-9]{4}-"
        + "[1-5]"
        + "[a-f0-9]{3}-"
        + "[89ab][a-f0-9]{3}-"
        + "[a-f0-9]{12}$"
    ),
    re.IGNORECASE,
)

UUID_GET = {3: d.uuid3, 5: d.uuid5}


class GenID(commands.Cog):
    """A cog wrapper for both ULID and UUID"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="objectid")
    async def _objectid(self, ctx: commands.Context):
        """Generates new bson.ObjectId"""
        await ctx.send(f"ObjectId: `{str(ObjectId())}`")

    @commands.command(name="ulid")
    async def _ulid(self, ctx: commands.Context):
        """Generates a new ULID"""
        await ctx.send(f"ULID: `{u.new().str}`")

    @commands.command(name="uuid2ulid")
    async def _uuid2ulid(self, ctx: commands.Context, uuid):
        """Converts a UUID to a ULID"""
        if UUID_VERIFY.match(uuid):
            ulid = u.parse(uuid)
            await ctx.send(f"ULID: `{ulid.str}`")
        else:
            await ctx.send(f"**Error:** UUID not valid")

    @commands.command(name="ulid2uuid")
    async def _ulid2uuid(self, ctx: commands.Context, ulid):
        """Converts a ULID to a UUID"""
        if ULID_VERIFY.match(ulid):
            ulid = u.parse(ulid)
            await ctx.send(f"UUID: `{ulid.uuid}`")
        else:
            await ctx.send(f"**Error:** ULID not valid.")

    @commands.command(name="uuid")
    async def _uuid(self, ctx: commands.Context, version=None, data=None):
        """Generates a new UUID based on version"""
        if not version:
            await ctx.send("**Error:** UUID version required\nAllowed versions: 3,4,5")
            return
        try:
            version = int(version)
            if version not in [3, 4, 5]:
                raise ValueError
        except (ValueError, TypeError):
            await ctx.send(
                f"**Error:** Invalid version {version}." + "Allowed versions: 3,4,5"
            )
            return
        if version in [3, 5] and not data:
            await ctx.send("**Error:** Data required for UUID3/5")
            return
        if version == 4:
            await ctx.send(f"UUID4: `{d.uuid4()}`")
        else:
            to_send = None
            if OID_VERIFY.match(data):
                to_send = UUID_GET[version](d.NAMESPACE_OID, data)
            elif URL_VERIFY.match(data):
                to_send = UUID_GET[version](d.NAMESPACE_URL, data)
            elif DN_VERIFY.match(data):
                to_send = UUID_GET[version](d.NAMESPACE_X500, data)
            else:
                to_send = UUID_GET[version](d.NAMESPACE_DNS, data)
            await ctx.send(f"UUID{version}: `{to_send}`")
