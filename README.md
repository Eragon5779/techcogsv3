# TechCogsV3

Technology-related cogs for use with [Red-DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot).

Meant to be used with Red's downloader by adding it with:

`[p]repo add TechCogsV3 https://gitlab.com/Eragon5779/techcogsv3.git`

## Cogs

### CPUCompare

A CPU specsheet lookup. Utilizes [Intel ARK](http://ark.intel.com) and [SpecDB](https://beta.specdb.info) for file creation.

** Will be updated every time CPUs are released **

#### Prerequisites
* pip packages
  * `discord`

#### Commands

* `[p]compare [brand1] [cpu1] [brand2] [cpu2]`
  * Finds and compares 2 CPUs. Prompts for missing info
* `[p]intel [arg]`
  * Finds an Intel CPU based on the search argument
* `[p]amd [arg]`
  * Finds an AMD CPU based on the search argument
* `[p]geekbench [arg]`
  * Gets the GeekBench scores based on the CPU entered

### Hash

A wrapper for Python `hashlib`. Supports all guaranteed algorithms from the library.

Is capable of hashing both files and text data.

#### Prerequisites

* pip packages
  * `aiohttp`
  * `discord`

#### Commands

* `[p]hash <algo> <hash_data>`
  * Hashes data using the supplied algorithm and data.
  * `hash_data` not needed if file is attached.
  * Supported algorithms:
    * md5
    * sha1
    * sha224
    * sha256
    * sha384
    * sha3_224
    * sha3_256
    * sha3_384
    * sha3_512
    * sha512
    * shake_128
    * shake_256

### Encode

Wrapper for Python `base64` library.

Can encode and decode all methods provided by this library.

#### Prerequisites

* pip packages
  * `discord`

#### Commands


* `[p] encode <method> <data>`
  * Encodes data using specified method
  * Supported methods
    * a85
    * b16
    * b32
    * b64
    * b85
* `[p]decode <method> <data>`
  * Decodes data using specified method
  * Supported methods
    * a85
    * b16
    * b32
    * b64
    * b85

### GenID

Wrapper for Python `uuid`, `bson.ObjectId` and [ulid-py](https://github.com/ahawker) libraries

Supports UUID version 3 through 5. UUID1 not supported due to needing MAC address

[What is a ULID?](https://github.com/ulid/spec)

#### Prerequisites

* pip packages
  * `ulid-py`
  * `bson`

#### Commands

* `[p]ulid`
  * Generates a new ULID
* `[p]uuid <version> <data>`
  * Generates a new UUID based on `version`
  * `data` required for versions 1 and 3
    * The cog will determine what type of namespace the data is as per the UUID library
* `[p]ulid2uuid <ulid>`
  * Converts the provided `ulid` to a UUID
* `[p]uuid2ulid <uuid>`
  * Converts the provided `uuid` to a ULID
* `[p]objectid`
  * Generates new bson ObjectId

## Contact Information

Discord Username: `eragon5779#5779`

Discord Server: `discord.gg/red`
